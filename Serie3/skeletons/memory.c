/* gcc -Wall ./memory.c -o memory */

/* Author: Julian Weyermann, 16-119-455 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* 1 KB = 1024bytes (See exercise)
Pagesize = 4KB
Define pSize as global variable */
int pSize = 1024*4;

/* 
 * System used: Linux Mint 4.15.0-47-generic x86_64 GNU/Linux
 * As this is using uint32_t it's OS independent since uint32_t holds a 32bit integer value on any OS. 
 */

int calculate_page (int32_t address) {
	return (int)address/pSize;
}

int calculate_offset (int32_t address , int page) {
	return address-(page*pSize);
}

int main(int argc, char *argv[])
{
	long long_input;
	int32_t address;
	int offset , page;
	

	if (argc != 2) {
		printf("You must provide a parameter as input!\n");
		return -1;
	} else {
		long_input = atol(argv[1]);
		long supremum = pow(2 , 8*sizeof(int32_t));
		if (long_input < 0 || long_input >= supremum) {
			printf("You must enter a valid 32bit address as parameter!\n");
			return -1;
		} else {
			address = (int32_t) long_input;
		}
	}
	
	/* Calculate values and print result */
	page = calculate_page(address);
	offset = calculate_offset(address, page);
	printf("The address %i contains:\npage number = %i\noffset = %i\n", address, page, offset);

	return 0;
}
