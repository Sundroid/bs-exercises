/* Julian Weyermann, 16-119-455 */

/* gcc -Wall net.c */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

uint8_t checkNumber(uint8_t number){
	if (number >= 0 && number <= 10)
		return 1;	
	else return 0;
}

int main(int argc, char *argv[])
{
	uint8_t num0 = (uint8_t) atoi(argv[1]);
	uint8_t num1 = (uint8_t) atoi(argv[2]);
	uint8_t sum;
	
	if (argc == 3){
		if (checkNumber(num0)==1 && checkNumber(num1)==1){
			sum = num0*num1;
		}
		else
		{
			printf("One of your numbers does not lie between 0 and 9! \n");
			exit(0);
		}
	}
	else
	{
		printf("Too many arguments! \n");
		exit(0);
	}	

	printf("%u * %u = %u\n", num0, num1, sum);

	return 0;
}
