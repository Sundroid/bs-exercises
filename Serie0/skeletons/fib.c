/* Julian Weyermann, 16-119-455 */

/* gcc -Wall -O2 fib.c */

#include <stdio.h>
#include <stdint.h>

int n = 0;


int fib(int a) {
    if(a == 0)
        return 0;
    else if(a == 1)
        return 1;
    else
        return (fib(a - 1) + fib(a - 2));
}

int main(void)
{
	n=10;
	printf("10. Fibonacci Zahl %d \n", fib(n));	

	return 0;
}
