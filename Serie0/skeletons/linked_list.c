/* Julian Weyermann, 16-119-455 */

/* gcc -Wall -O2 linked_list.c */
/* valgrind ./a.out */

#include <stdio.h>
#include <stdlib.h>

struct elem {
	int pos;
	struct elem *next;
};

static void panic(const char *serror)
{
	printf("%s", serror);
	exit(1);
}

static void *xmalloc(size_t size)
{
	void *ptr;
	if (size == 0)
		panic("Size is 0!\n");
	ptr = malloc(size);
	if (!ptr)
		panic("No mem left!\n");
	return ptr;
}

static void xfree(void *ptr)
{
	if (ptr == NULL)
		panic("Got Null-Pointer!\n");
	free(ptr);
}

static struct elem *init_list(size_t len)
{
	struct elem *current;
	// Ja, ich weiss, ich könnte direkt "struct elem *current = xmalloc(sizeof(struct elem))" schreiben. Allerdings half mir diese schreibweise hier beim Verständnis!
	current = xmalloc(sizeof(struct elem));
	struct elem *next;
	struct elem *first;
	first = current;
	
	int i = 1;
	while (i<=len){
		next = xmalloc(sizeof(struct elem));
		current -> pos = i;
		current -> next = next;
		
		//Das letzte Element muss wieder auf das erste zeigen
		if (i == len){
			current -> next = first;
		}
		current = next;
		i++;
	}
	
	return first;
}

static void clean_list(struct elem *head, size_t len)
{
	struct elem *tmp;
	struct elem *current;
	current = head;
	int i = 0;
	while (i < len){
		tmp = current;
		current = current -> next;
		xfree(tmp);
		i++;
	}
	
	head = NULL;
		
}

static void traverse_list(struct elem *head, int times)
{
	struct elem *current;
	current	= head;
	int i = 0;
	while (i < times){
		do{
			printf("Iteration %d, element %d \n", i+1, current->pos);
			current = current -> next;
		} while(current != head);
		i++;
	}
}

int main(void)
{
	struct elem *head = NULL;
	size_t len = 10;

	head = init_list(len);
	traverse_list(head, 2);
	clean_list(head, len);

	return 0;
}

