/* Julian Weyermann*/
/* 16-119-455 */
/* gcc -Wall -pthread statistics.c -o statistics */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int max;
int min;
float avg;
int len;

void *average(void *val) {
	int* vals = (int *) val;
	
	/*add up all values in the array*/
	for (int i = 0; i < len; i++){
		avg+=vals[i];
	}
	
	/*divide sum by number of values*/
	avg = avg/(long)len;
	
	return NULL;
}

void *minimum(void *val) {
	int* vals = (int *) val;
	
	/*set first value of array as min*/
	min = vals[0];
	
	/*compare other values of array to previously set min and if the value is bigger, replace min*/
	for (int i = 1; i < len; i++){
		if (vals[i]<min){
			min=vals[i];
		}
	}
	
	return NULL;
}

void *maximum(void *val) {
	int* vals = (int *) val;
	
	/*set first value of array as max*/
	max = vals[0];
	
	/*compare other values of array to previously set max and if the value is bigger, replace max*/
	for (int i = 1; i < len; i++){
		if (vals[i]>max){
			max=vals[i];
		}
	}
	
	return NULL;
}

int main(int argc, char *argv[])
{
	int *array; // use this to store the command line parameters as integers
	pthread_t thread0;
	pthread_t thread1;
	pthread_t thread2;

	if (argc < 3) {
		printf("You have to supply at least 2 integers as arguments!\n");
		return 0;
	}

	/*calculate number of values entered*/
	len = argc - 1;
	
	
	char* input_string;
	
	/*allocate memory according to number of input values*/
	array = (int *) malloc(len*sizeof(int));
	
	for (int i = 1; i <argc; i++){
		/*convert string to long int with base 10 in order to convert it to int value later*/
		strtol(argv[i], &input_string, 10);
		if(!*input_string){
			array[i-1] = atoi(argv[i]);
		}else{
			printf("Input invalid, only integer values are accepted!\n");
			return 0;
		}
	}

	pthread_create(&thread0,NULL,average,(void*) array);
    pthread_create(&thread1,NULL,minimum,(void*) array);
    pthread_create(&thread2,NULL,maximum,(void*) array);

    pthread_join(thread0,NULL);
    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);

		
	printf("Average: %f\n" , avg);
	printf("Maximum: %i\n" , max);
	printf("Minimum: %i\n" , min);

	return 0;
}
