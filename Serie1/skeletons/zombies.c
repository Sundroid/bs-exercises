/* Julian Weyermann*/
/* 16-119-455 */
/* gcc -Wall -std=c99 ./zombies.c -o zombies */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

void create_zombie() {
	
	exit(0);
	
	return;
}

int main(void)
{
	int wait = 15;
	
	/* Create fork-process and kill it immediately again, then put programm to sleep, before the killed fork gets closed */
	pid_t pid_fork = fork();
	
	if (pid_fork == 0){
		create_zombie();
	}
	
	sleep(wait);
	
	return 0;
}
